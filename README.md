nvOC MiningPoolHub Switcher
===========================

nvOC makes it easy to squeeze more hashrate out of your GPUs.  MiningPoolHub
makes it easy to mine altcoins and get paid in Bitcoin.  However, nvOC is a
customized Ubuntu Linux and the management software MiningPoolHub recommends
for profit switching only runs under Windows.  Their approach to
coin-switching with nVidia GPUs under Linux is to loop through a bunch of
different miners, erroring out on all but the currently-profitable coin.  I
lost nearly four hours this morning when a miner didn't exit properly during
a switch.

This set of scripts bridges the gap.  With them, you can use the miners
included in nvOC (and others that you might add) to mine whatever's most
profitable at MiningPoolHub.  For each supported algorithm, you can
customize power, clockspeed, and fan speed settings as you consider
appropriate.  

Setup
-----

1. ssh into your mining rig's m1 account.
2. Clone these files into the same directory as nvOC's oneBash script.
3. Disable oneBash by commenting out all lines in ~/2unix.  oneBash should
be invoked manually if you add/remove GPUs so it can reconfigure
/etc/X11/xorg.conf, but it otherwise isn't used.  To keep gnome-terminal
from sucking down CPU cycles by constantly restarting the 2unix script, you
might want to include something like "sleep 86400" at the end.
4. Edit mph_conf.json.  You'll set your user and miner names here.  You can 
also customize your overclocking settings for each algorithm here.
5. If your network is IPv6-enabled, note that nvOC (as of v17) has a problem
connecting to IPv6 hosts.  MiningPoolHub makes its API available over both IPv6
and IPv4, so to force it to use IPv4 in the meantime, edit /etc/hosts as
follows:

    `104.27.168.52 miningpoolhub.com`
6. Set mph_switch.py to launch with a cronjob:

    `0,10,20,30,40,50 * * * * (cd /media/m1/1263-A96E && python2.7 mph_switch.py mph_conf.json)`

Every 10 minutes, the script will run and determine which miner needs to
start up.  If it's already running, it will leave it alone.  If it needs to
switch, it kills one miner and starts the other.  If nothing's running
(because the rig just rebooted or the miner crashed), it will start the
miner.

current-profit is a text file that will be updated on every run of switch.py
to show which is currently the most profitable.  Profitability is determined
by the MiningPoolHub API and is a unitless number; higher is better.

algo-log is a list of coins and algorithms the switcher has selected when
run.  If you start seeing repeats of the same algo every 10 minutes, that's
a sign that something has gone wrong with your GPUs and you might want to
consider a reboot.

Miners are started in Screen sessions (http://www.gnu.org/software/screen)
that can be viewed over an ssh login session:

`screen -dr miner`

When the rig switches algorithms, the Screen session will be closed and a
new one opened.  Repeat the above to monitor it, or you could use something
like this on a monitoring workstation:

`while true; do screen -dr miner; done`

Adding Algorithms
-----------------

Some algorithms supported by MiningPoolHub can't be mined with the miners
included in nvOC.  If you add the appropriate software, though, you can mine
these as well.  Instructions to add these capabilities will follow.

Sia
---

To add the miner:

`sudo apt install golang-go
mkdir /home/m1/gocode
GOPATH=/home/m1/gocode go get github.com/robvanmieghem/gominer`

To enable mining, create a new object named "sia" within the "miners"
list in mph_conf.json.  Here's what I use:

```
 "miners": 
 {
   [existing entries deleted for brevity]
    "Sia":
    {
      "bin": "/home/m1/SPccminer/ccminer -a sia -o stratum+tcp://{HOST}:{PORT} -u {NAME}.{MINER} -p x",
      "power_limit": [115,115,95], "gpu_oc": [-100,-100,0], "mem_oc": 0, "fan": [70,75,70]
    }
 }
```

CryptoNight
-----------

To add the miner:

```
cd ~
git clone https://github.com/KlausT/ccminer-cryptonight KTccminer-cryptonight
cd KTccminer-cryptonight
TAB="$(printf '\t')"
cat <<EOF | patch
--- Makefile.am 2017-07-26 17:32:02.000000000 -0700
+++ Makefile.am 2017-09-21 12:33:54.412247785 -0700
@@ -33,7 +33,7 @@
 ccminer_LDADD${TAB}${TAB}= @LIBCURL@ @JANSSON_LIBS@ @PTHREAD_LIBS@ @WS2_LIBS@ @CUDA_LIBS@ @OPENMP_CFLAGS@ @LIBS@
 ccminer_CPPFLAGS${TAB}= \$(CUDA_CFLAGS) -msse2 @LIBCURL_CPPFLAGS@ @OPENMP_CFLAGS@ \$(PTHREAD_FLAGS) -fno-strict-aliasing \$(JANSSON_INCLUDES) -DSCRYPT_KECCAK512 -DSCRYPT_CHACHA -DSCRYPT_CHOOSE_COMPILETIME

-NVCC_GENCODE${TAB}${TAB}= -D_FORCE_INLINES \$(CUDA_CFLAGS) -gencode=arch=compute_20,code=sm_20 -gencode=arch=compute_20,code=sm_21 -gencode=arch=compute_30,code=sm_30 -gencode=arch=compute_35,code=sm_35 -gencode=arch=compute_37,code=sm_37 -gencode=arch=compute_50,code=sm_50  -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_61,code=sm_61
+NVCC_GENCODE${TAB}${TAB}= -D_FORCE_INLINES \$(CUDA_CFLAGS) -gencode=arch=compute_61,code=sm_61

 # we're now targeting all major compute architectures within one binary.
 .cu.o:
EOF
./autogen.sh
./configure --with-cuda=/usr/local/cuda-8.0
make
```

To enable mining, create a new object named "cryptonight" within the "miners"
list in mph_conf.json.  Here's what I use:

```
 "miners": 
 {
   [existing entries deleted for brevity]
    "Cryptonight":
    {
      "bin": "/home/m1/KTccminer-cryptonight/ccminer -a cryptonight -o stratum+tcp://{HOST}:{PORT} -u {NAME}.{MINER} -p x",
      "power_limit": [115,115,95], "gpu_oc": -200, "mem_oc": 1000, "fan": [70,75,70]
    }
 }
```
